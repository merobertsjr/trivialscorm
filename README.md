# Trivial SCORM Player

For reasons unknown, SCORM RTEs and documentation available online are super complex.

This is not a fully-featured implementation but just a simplest way to run SCORM packages in a browser.

It has been developed for specific purpose and tested on packages generated in Adobe Captivate and most probably wont work with other packages.

See "example.html" for usage.

For a complete implementation go to http://www.vsscorm.net/
